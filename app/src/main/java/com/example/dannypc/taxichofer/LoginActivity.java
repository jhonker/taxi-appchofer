package com.example.dannypc.taxichofer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dannypc.taxichofer.utils.JSON;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    String valor ,nombreChofer;
    String idChofer;
    EditText clave,user;
    Button btn_login;
    RequestQueue queue;

    ProgressDialog progressDialog;
    private SharedPreferences session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        session=getSharedPreferences("prefent",Context.MODE_PRIVATE);


        String beta= session.getString("id","");
        if(beta.equals("ok")){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }




        clave = findViewById(R.id.et_codigo);
        btn_login=findViewById(R.id.btn_iniciar);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clave.getText().toString().equals("")){
                        clave.setError("Por favor este campo es necesario");
                }else{
                    new logear(LoginActivity.this).execute();
                }
            }
        });


    }
    class logear extends AsyncTask<String,String,String> {
        private Activity context;
        logear(Activity context){

            this.context=context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Comprobando su clave");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.hide();
            if(s.equals("ok")){
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else{
                StyleableToast.makeText(LoginActivity.this, "La clave es incorrecta!", Toast.LENGTH_LONG, R.style.StyledToastError).show();

            }


        }

        protected String doInBackground(String... params) {
            String resul="error";
            HttpClient httpClient;
            List<NameValuePair> nameValuePairs;
            HttpPost httpPost;
            httpClient = new DefaultHttpClient();
            httpPost = new HttpPost("http://"+JSON.ipserver +"/api/logmovilchofer");//url del servidor
            //empezamos añadir nuestros datos
            nameValuePairs = new ArrayList<NameValuePair>(4);
            nameValuePairs.add(new BasicNameValuePair("codigo",clave.getText().toString().trim()));

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                response.getStatusLine();
                String values = EntityUtils.toString(entity);
                JSONObject a= new JSONObject(values);
                valor= a.getString("success");
                nombreChofer=a.getString("nombres");
                idChofer=a.getString("id");

                SharedPreferences.Editor editor = session.edit();
                editor.putString("id", valor);
                editor.putString("nombrechofer",nombreChofer);
                editor.putString("idchofer",idChofer);

                //editor.commit();//sincrono
                editor.apply();//asyncrono

                resul ="ok";
            } catch(UnsupportedEncodingException e){
                e.printStackTrace();
            }catch (ClientProtocolException e){
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return  resul;
        }
    }


}
